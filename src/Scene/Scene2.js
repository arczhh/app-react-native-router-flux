import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'; // New code


const image = require('../Assets/images/cat.jpeg');

export default class Scene2 extends Component {

    render() {
        return (
            <View>
                <Image source={image} />
            </View>
        )
    }
}
