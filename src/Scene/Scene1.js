import React, { Component } from 'react'
import { Text, View, Button } from 'react-native'
import { Actions } from 'react-native-router-flux'; // New code

export default class Scene1 extends Component {

    componentDidMount() {

    }

    render() {
        return (
            <View>
                <Text> textInComponent </Text>
                <Button title={"Click me"} onPress={() => { Actions.scene2() }} />
            </View>
        )
    }
}
