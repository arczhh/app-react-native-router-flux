import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import Scene1 from './Scene/Scene1'
import Scene2 from './Scene/Scene2'


const Navigation = () => {
    return (
        <Router>
            <Scene key="root">
                <Scene key="scene1" component={Scene1} title="Scene1" initial />
                <Scene key="scene2" component={Scene2} title="Scene2" />
            </Scene>
        </Router>
    );
}

export default Navigation;