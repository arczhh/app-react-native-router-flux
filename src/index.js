import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Navigation from './navigation'

const App = () => {
    return (
        <Navigation />
    );
}

export default App;